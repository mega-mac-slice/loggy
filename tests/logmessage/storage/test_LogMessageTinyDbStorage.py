import os
from unittest import TestCase

from loggy.logmessage.model import LogMessageStorageSearchRequest
from loggy.logmessage.storage import LogMessageStorage, LogMessageTinyDbStorage
from tests.test.mixin import LogMessageMixin


class TestLogMessageTinyDbStorage(TestCase, LogMessageMixin):

    def test_create_and_search(self):
        storage = self._create_storage()
        messages = [
            self.create_message(),
        ]
        storage.create(messages)
        results = storage.search(LogMessageStorageSearchRequest(level="INFO"))
        self.assertEqual(1, len(results), "we expect 1 INFO message")
        results = storage.search(LogMessageStorageSearchRequest(level="WARN"))
        self.assertEqual(0, len(results), "we expect to filter out the INFO message")
        results = storage.search(LogMessageStorageSearchRequest())
        self.assertEqual(
            1,
            len(results),
            "we expect to search across everything when no filter is provided",
        )

    def test_create_and_search_timerange(self):
        storage = self._create_storage()
        messages = [self.create_message(timestamp=i) for i in range(10)]
        storage.create(messages)
        results = storage.search(LogMessageStorageSearchRequest(start=5))
        self.assertEqual([5, 6, 7, 8, 9], [x.timestamp for x in results])
        results = storage.search(LogMessageStorageSearchRequest(end=5))
        self.assertEqual([0, 1, 2, 3, 4], [x.timestamp for x in results])
        results = storage.search(LogMessageStorageSearchRequest(start=4, end=6))
        self.assertEqual([4, 5], [x.timestamp for x in results])

    def test_create_and_search_and_filtering(self):
        storage = self._create_storage()
        storage.create(
            [self.create_message(level="INFO", module="api") for _ in range(3)]
        )
        storage.create(
            [self.create_message(level="ERROR", module="api") for _ in range(3)]
        )
        storage.create(
            [self.create_message(level="ERROR", module="scheduler") for _ in range(3)]
        )

        results = storage.search(
            LogMessageStorageSearchRequest(level="INFO", module="api")
        )
        self.assertEqual(
            3, len(results), "we expect 3 INFO messages from the api module"
        )

    def _create_storage(self) -> LogMessageStorage:
        os.makedirs("/tmp/loggy", exist_ok=True)
        return LogMessageTinyDbStorage(db_path="/tmp/loggy/test.db.json")

    def _nuke_storage(self):
        path = "/tmp/loggy/test.db.json"
        if os.path.exists(path):
            os.remove("/tmp/loggy/test.db.json")

    def setUp(self):
        self._nuke_storage()

    def tearDown(self):
        self._nuke_storage()
