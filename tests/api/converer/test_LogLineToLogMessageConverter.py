from unittest import TestCase

from loggy.api.converter.LogLineApiToStorageConverter import (
    LogLineApiToStorageConverter,
)


class TestLogLineToLogMessageConverter(TestCase):
    def test_convert_info_1(self):
        message = "2019-10-12T07:20:50.52Z INFO api - This is an INFO level log message"
        converter = self.create_converter()
        result = converter.convert(message)
        self.assertDictEqual(
            {
                "timestamp": 1570864850520,
                "level": "INFO",
                "module": "api",
                "message": "This is an INFO level log message",
            },
            result.model_dump(),
        )

    def test_convert_error_1(self):
        message = (
            "2019-10-12T07:20:50.52Z ERROR runner - This is an ERROR level log message!"
        )
        converter = self.create_converter()
        result = converter.convert(message)
        self.assertDictEqual(
            {
                "timestamp": 1570864850520,
                "level": "ERROR",
                "module": "runner",
                "message": "This is an ERROR level log message!",
            },
            result.model_dump(),
        )

    def create_converter(self) -> LogLineApiToStorageConverter:
        return LogLineApiToStorageConverter()
