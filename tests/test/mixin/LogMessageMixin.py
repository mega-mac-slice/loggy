from loggy.logmessage.model import LogMessage


class LogMessageMixin:
    raw_message_info = (
        "2019-10-12T07:20:50.52Z INFO api - This is an INFO level log message!"
    )

    def create_message(
        self,
        timestamp: int = 1570864850520,
        level: str = "INFO",
        module="api",
        message: str = "This is an INFO level log message",
    ):
        return LogMessage(
            timestamp=timestamp,
            level=level,
            module=module,
            message=message,
        )
