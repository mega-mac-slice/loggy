from unittest import TestCase

from fastapi.testclient import TestClient

from loggy.app import app
from tests.test.mixin import LogMessageMixin


class TestApp(TestCase, LogMessageMixin):
    client = TestClient(app)

    def test_create_logs(self):
        response = self.client.post(
            "/logs",
            json={"items": [{"message": self.raw_message_info}]},
        )
        assert response.status_code == 200

    def test_create_logs_fail_on_message_only(self):
        response = self.client.post(
            "/logs",
            json={"items": [{"message": "hello world"}]},
        )
        assert response.status_code == 400

    def test_create_logs_fail_on_bad_timestamp(self):
        response = self.client.post(
            "/logs",
            json={
                "items": [
                    {
                        "message": "1719197346 INFO api - This is an INFO level log message!"
                    }
                ]
            },
        )
        assert response.status_code == 400

    def test_search_logs(self):
        response = self.client.post("/logs/search", json={"level": "INFO"})
        assert response.status_code == 200

    def test_search_logs_empty(self):
        response = self.client.post(
            "/logs/search",
        )
        assert response.status_code == 200

    def test_delete_logs_by_search_empty(self):
        response = self.client.delete(
            "/logs/search",
        )
        assert response.status_code == 200
