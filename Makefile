.PHONY: clean dist
APP_NAME=$(shell ./setup.py --name)
APP_VERSION=$(shell ./setup.py --version)

dev:
	python3 -m venv .venv

install:
	.venv/bin/pip install --pre -e .[dev]

format:
	.venv/bin/isort --profile black setup.py loggy tests && .venv/bin/black setup.py loggy tests

typecheck:
	.venv/bin/mypy loggy

lint:
	.venv/bin/pycodestyle loggy tests setup.py

check: format typecheck lint

test:
	.venv/bin/pytest

start:
	.venv/bin/fastapi dev loggy/app.py

clean:
	rm -rf .venv
	rm -rf loggy.egg-info .eggs .tox Pipfile*

