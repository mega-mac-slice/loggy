import logging
import os
from typing import List, Optional

from fastapi import FastAPI

from loggy.api import LogsApiController
from loggy.api.model import (
    CreateLogsRequest,
    DeleteLogsBySearchRequest,
    DeleteLogsBySearchResponse,
    SearchLogsRequest,
    SearchLogsRequestItem,
    SearchLogsResponse,
)
from loggy.logmessage.model import LogMessage
from loggy.logmessage.storage import LogMessageStorage, LogMessageTinyDbStorage

logging.basicConfig(
    format="[%(levelname)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.DEBUG,
)
logger = logging.getLogger()

app = FastAPI()

os.makedirs("/tmp/loggy", exist_ok=True)
storage: LogMessageStorage = LogMessageTinyDbStorage(db_path="/tmp/loggy/local.db.json")

logs_api_controller: LogsApiController = LogsApiController(storage=storage)


@app.post("/logs")
def create_logs(request: CreateLogsRequest):
    messages: List[str] = [item.message for item in request.items]
    logs_api_controller.add_messages(messages)


@app.post("/logs/search")
def search_logs(request: Optional[SearchLogsRequest] = None) -> SearchLogsResponse:
    search_request: SearchLogsRequest = request or SearchLogsRequest()
    hits: List[LogMessage] = logs_api_controller.search_messages(search_request)
    items: List[SearchLogsRequestItem] = [
        SearchLogsRequestItem(message=item.message) for item in hits
    ]
    result: SearchLogsResponse = SearchLogsResponse(items=items)

    return result


@app.delete("/logs/search")
def delete_logs_by_search(
    request: Optional[DeleteLogsBySearchRequest] = None,
) -> DeleteLogsBySearchResponse:
    delete_request: DeleteLogsBySearchRequest = request or DeleteLogsBySearchRequest()
    deleted: int = logs_api_controller.delete_messages_by_search(delete_request)
    return DeleteLogsBySearchResponse(deleted=deleted)


def main():
    pass


if __name__ == "__main__":
    main()
