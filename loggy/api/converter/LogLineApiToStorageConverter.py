from dateutil import parser
from dateutil.parser import ParserError

from loggy.api.exception import LogLineConversionError
from loggy.logmessage.model.LogMessage import LogMessage


class LogLineApiToStorageConverter:
    """
    <datetime in iso rfc3339 format> <log level> <module> - <message>
    """

    def convert(self, log_line) -> LogMessage:
        try:
            assert " - " in log_line, "No ' - ' delimiter found."
            first_half, message = log_line.split(" - ", 1)

            timestamp, level, module = first_half.split(" ")
            assert timestamp, "No timestamp found."
            assert level, "No level found."
            assert module, "No module found."

            epoch = int(parser.parse(timestamp).timestamp() * 1000)
        except (AssertionError, ParserError) as ex:
            raise LogLineConversionError(
                f"Invalid log line - {ex} - Supported format: <datetime in iso rfc3339 format> <log level> <module> - <message>"
            )

        return LogMessage(
            timestamp=epoch,
            level=level,
            module=module,
            message=message,
        )
