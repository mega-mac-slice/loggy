from loggy.api.model.DeleteLogsBySearchRequest import DeleteLogsBySearchRequest
from loggy.logmessage.model.LogMessageStorageDeleteBySearchRequest import (
    LogMessageStorageDeleteBySearchRequest,
)


class DeleteLogsBySearchRequestApiToStorageConverter:
    def convert(
        self, request: DeleteLogsBySearchRequest
    ) -> LogMessageStorageDeleteBySearchRequest:
        return LogMessageStorageDeleteBySearchRequest(
            start=request.start,
            end=request.end,
            level=request.level,
            module=request.module,
            message=request.message,
        )
