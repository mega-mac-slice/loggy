from loggy.api.model.SearchLogsRequest import SearchLogsRequest
from loggy.logmessage.model.LogMessageStorageSearchRequest import (
    LogMessageStorageSearchRequest,
)


class SearchLogsRequestApiToStorageConverter:
    def convert(self, request: SearchLogsRequest) -> LogMessageStorageSearchRequest:
        return LogMessageStorageSearchRequest(
            start=request.start,
            end=request.end,
            level=request.level,
            module=request.module,
            message=request.message,
        )
