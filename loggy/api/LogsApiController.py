from typing import List

from loggy.api.converter import (
    DeleteLogsBySearchRequestApiToStorageConverter,
    LogLineApiToStorageConverter,
    SearchLogsRequestApiToStorageConverter,
)
from loggy.api.model import DeleteLogsBySearchRequest, SearchLogsRequest
from loggy.logmessage.model.LogMessage import LogMessage
from loggy.logmessage.storage.LogMessageStorage import LogMessageStorage


class LogsApiController:

    def __init__(self, storage: LogMessageStorage):
        self._storage: LogMessageStorage = storage
        self._log_line_converter: LogLineApiToStorageConverter = (
            LogLineApiToStorageConverter()
        )
        self._search_request_convertor: SearchLogsRequestApiToStorageConverter = (
            SearchLogsRequestApiToStorageConverter()
        )

        self._delete_request_convertor: (
            DeleteLogsBySearchRequestApiToStorageConverter
        ) = DeleteLogsBySearchRequestApiToStorageConverter()

    def add_messages(self, message: List[str]) -> None:
        messages = [self._log_line_converter.convert(log_line) for log_line in message]
        self._storage.create(messages)

    def search_messages(self, api_request: SearchLogsRequest) -> List[LogMessage]:
        storage_request = self._search_request_convertor.convert(api_request)
        return self._storage.search(storage_request)

    def delete_messages_by_search(self, api_request: DeleteLogsBySearchRequest) -> int:
        storage_request = self._delete_request_convertor.convert(api_request)
        return self._storage.delete_by_search(storage_request)
