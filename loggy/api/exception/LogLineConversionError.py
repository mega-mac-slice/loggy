from fastapi import HTTPException


class LogLineConversionError(HTTPException):
    def __init__(self, message):
        self.message = message
        super().__init__(status_code=400, detail=self.message)
