from typing import Optional

from pydantic import BaseModel, Field


class SearchLogsRequest(BaseModel):
    start: Optional[int] = Field(None, description="Epoch time for start")
    end: Optional[int] = Field(None, description="Epoch time for end")
    level: Optional[str] = Field(None, description="The logging level")
    module: Optional[str] = Field(
        None, description="The module where the log originates"
    )
    message: Optional[str] = Field(None, description="The log message")
