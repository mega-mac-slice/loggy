from typing import List, Optional

from pydantic import BaseModel, Field


class SearchLogsRequestItem(BaseModel):
    message: str


class SearchLogsResponse(BaseModel):
    items: List[SearchLogsRequestItem]
