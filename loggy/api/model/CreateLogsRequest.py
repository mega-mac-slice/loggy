from typing import List

from pydantic import BaseModel


class CreateLogsRequestItem(BaseModel):
    message: str


class CreateLogsRequest(BaseModel):
    items: List[CreateLogsRequestItem]
