from typing import Optional

from pydantic import BaseModel, Field

from loggy.api.model.SearchLogsRequest import SearchLogsRequest


class DeleteLogsBySearchResponse(BaseModel):
    deleted: int = Field(description="The number of logs deleted")
