from loggy.logmessage.model.LogMessageStorageSearchRequest import (
    LogMessageStorageSearchRequest,
)


class LogMessageStorageDeleteBySearchRequest(LogMessageStorageSearchRequest):
    pass
