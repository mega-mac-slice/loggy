from pydantic import BaseModel, Field


class LogMessage(BaseModel):
    timestamp: int = Field(description="The timestamp of the log message")
    level: str = Field(description="The logging level")
    module: str = Field(description="The module where the log originates")
    message: str = Field(description="The log message")
