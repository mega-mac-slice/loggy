from typing import Optional

from pydantic import BaseModel


class LogMessageStorageSearchRequest(BaseModel):
    start: Optional[int] = None
    end: Optional[int] = None
    level: Optional[str] = None
    module: Optional[str] = None
    message: Optional[str] = None
