from abc import ABC, abstractmethod
from typing import List

from loggy.logmessage.model import (
    LogMessage,
    LogMessageStorageDeleteBySearchRequest,
    LogMessageStorageSearchRequest,
)


class LogMessageStorageRead(ABC):
    @abstractmethod
    def search(self, request: LogMessageStorageSearchRequest) -> List[LogMessage]:
        pass


class LogMessageStorageWrite(ABC):
    @abstractmethod
    def create(self, log_messages: List[LogMessage]):
        pass

    @abstractmethod
    def delete_by_search(self, request: LogMessageStorageDeleteBySearchRequest) -> int:
        pass


class LogMessageStorage(LogMessageStorageRead, LogMessageStorageWrite):
    pass
