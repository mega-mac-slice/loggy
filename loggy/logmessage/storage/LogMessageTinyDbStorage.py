from typing import List, Union

from tinydb import Query, TinyDB
from tinydb.queries import QueryInstance

from loggy.logmessage.model import (
    LogMessage,
    LogMessageStorageDeleteBySearchRequest,
    LogMessageStorageSearchRequest,
)
from loggy.logmessage.storage.LogMessageStorage import LogMessageStorage


class LogMessageTinyDbStorage(LogMessageStorage):
    def __init__(self, db_path: str):
        self._db_path: str = db_path
        self._db: TinyDB = TinyDB(db_path)

    def search(self, request: LogMessageStorageSearchRequest) -> List[LogMessage]:
        query = self._create_query(request)
        docs = self._db.search(query)

        return [LogMessage.model_validate(doc) for doc in docs]

    def create(self, log_messages: List[LogMessage]):
        self._db.insert_multiple(
            [log_message.model_dump() for log_message in log_messages]
        )

    def delete_by_search(self, request: LogMessageStorageDeleteBySearchRequest) -> int:
        query = self._create_query(request)
        result = self._db.remove(query)
        return len(result)

    def _create_query(
        self,
        request: Union[
            LogMessageStorageSearchRequest, LogMessageStorageDeleteBySearchRequest
        ],
    ) -> QueryInstance:
        Q = Query()

        match = request.model_dump(include={"level", "module", "message"})
        match = {k: v for k, v in match.items() if v is not None}

        query = Q.fragment(match)

        if request.start is not None and request.end is not None:
            query = query & (Q.timestamp >= request.start) & (Q.timestamp < request.end)
        elif request.start is not None:
            query = query & (Q.timestamp >= request.start)
        elif request.end is not None:
            query = query & (Q.timestamp < request.end)

        return query
