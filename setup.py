#!/usr/bin/env python3
from setuptools import find_packages, setup

setup(
    name="mms-loggy",
    version="0.0.1",
    packages=find_packages(),
    url="https://gitlab.com/mega-mac-slice/loggy",
    license="MIT",
    author="mega-mac-slice",
    author_email="megamacslice@protonmail.com",
    description="A quick tech demo using FastAPI and TinyDB to create a simple CRUD service.",
    entry_points={"console_scripts": ["loggy = loggy.app:main"]},
    data_files=["README.md", "requirements.txt"],
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    install_requires=open("requirements.txt").readlines(),
    tests_require=["mock"],
    test_suite="tests",
    extras_require={
        "dev": [
            "black",
            "bumpversion",
            "isort",
            "mock",
            "mypy",
            "pycodestyle",
            "pytest",
            "tox",
            "types-python-dateutil",
            "twine",
        ]
    },
)
