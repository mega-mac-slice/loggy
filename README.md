# loggy
A quick tech demo using FastAPI and TinyDB to create a simple CRUD service.

### Development

## Requirements
- `python 3`

## Installation
```bash
make dev && make install
```

## Running
```
make start
```


## Reference
- [FastAPI](https://fastapi.tiangolo.com/)
- [TinyDB](https://tinydb.readthedocs.io/en/latest/)

